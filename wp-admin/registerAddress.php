<?php
/**
 * Tools Administration Screen.
 *
 * @package WordPress
 * @subpackage Administration
 */

/** WordPress Administration Bootstrap */
require_once( dirname( __FILE__ ) . '/admin.php' );

$title = __('Address Maps');

get_current_screen()->add_help_tab( array(
	'id'      => 'press-this',
	'title'   => __('Press This'),
	'content' => '<p>' . __('Press This is a bookmarklet that makes it easy to blog about something you come across on the web. You can use it to just grab a link, or to post an excerpt. Press This will even allow you to choose from images included on the page and use them in your post. Just drag the Press This link on this screen to your bookmarks bar in your browser, and you&#8217;ll be on your way to easier content creation. Clicking on it while on another website opens a popup window with all these options.') . '</p>',
) );
get_current_screen()->add_help_tab( array(
	'id'      => 'converter',
	'title'   => __('Categories and Tags Converter'),
	'content' => '<p>' . __('Categories have hierarchy, meaning that you can nest sub-categories. Tags do not have hierarchy and cannot be nested. Sometimes people start out using one on their posts, then later realize that the other would work better for their content.' ) . '</p>' .
	'<p>' . __( 'The Categories and Tags Converter link on this screen will take you to the Import screen, where that Converter is one of the plugins you can install. Once that plugin is installed, the Activate Plugin &amp; Run Importer link will take you to a screen where you can choose to convert tags into categories or vice versa.' ) . '</p>',
) );

get_current_screen()->set_help_sidebar(
	'<p><strong>' . __('For more information:') . '</strong></p>' .
	'<p>' . __('<a href="https://codex.wordpress.org/Tools_Screen">Documentation on Tools</a>') . '</p>' .
	'<p>' . __('<a href="https://wordpress.org/support/">Support Forums</a>') . '</p>'
);

require_once( ABSPATH . 'wp-admin/admin-header.php' );

?>
<div class="wrap">
<h1><?php echo esc_html( $title ); ?></h1>

<?php if ( current_user_can('edit_posts') ) : ?>
</div>
<?php
endif;

if ( current_user_can( 'import' ) ) :
$cats = get_taxonomy('category');
$tags = get_taxonomy('post_tag');
if ( current_user_can($cats->cap->manage_terms) || current_user_can($tags->cap->manage_terms) ) : ?>

<?php
endif;
endif;
?>

<form method="POST" action="actionAddress.php">
<?php 
  echo "<input type='text' name='name' placeholder='Nome Estabelecimento' id='nome' style='width:35%; height: 35px;'>";
  echo "<br>";
  echo "<input type='text' name='address' placeholder='Endereço' id='endereco' style='width:35%; height: 35px;'>";
  echo "</br>";
  echo "<input type='text' name='lat' placeholder='Latitude' id='lat' style='width:35%; height: 35px;'>";
  echo "</br>";
  echo "<input type='text' name='lng' placeholder='Longitude' id='long' style='width:35%; height: 35px;'>";
  echo "</br>";
  echo "<select name='type' style='width: 35%; height: 35px;'>";
  	echo "<option>Informe o tipo</option>"; 
  	echo "<option>Clínica</option>"; 
  echo "</select>";
  echo "</br></br>";
  echo "<input type='submit' class='button button-primary fw-pull-right fw-builder-header-post-save-button' id='btnSend' value='Registrar' />";
?>
</form>

<?php
/**
 * Fires at the end of the Tools Administration screen.
 *
 * @since 2.8.0
 */
do_action( 'tool_box' );
?>
</div>

<?php
include( ABSPATH . 'wp-admin/admin-footer.php' );
