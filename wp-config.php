<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'ingridix_system');


/** Usuário do banco de dados MySQL */
define('DB_USER', 'ingridix');


/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'P77DbIngridix');


/** Nome do host do MySQL */
define('DB_HOST', '54.233.114.244');


/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');


/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '.ll4j~:1RT&N^6~$v}_N*76Y_D$|}Y.C90iDo]E7@%>udZWdMO?n}}pnD49 (dH.');

define('SECURE_AUTH_KEY',  'By~.Rr?::$e?:@=QvsMGD}~w1D<q^$d?<{Ju;2~(lP,v1bY)+2.}kq1*B$/Ig -?');

define('LOGGED_IN_KEY',    '*4Ld~!Qv21O2O5P9RfWd.$2aM5cNFL>ohZ?>t-5!a>l :r.bAjR;wSMy_:K)t@N9');

define('NONCE_KEY',        'BAe,m]u4Wq!lUkvJf^}Al9Hq/?;h?o,Bcd?7m<gX(7au3/T[7wRj*?O}mv:pYm3j');

define('AUTH_SALT',        'e`8)xA~wOvp0 bFY8XC+#6uM4r2q8z_^SzHiP40Fn@RjxFf5^7dFMMp,1#plAR+*');

define('SECURE_AUTH_SALT', 'H^:3$P8RSHBFUcXL,dc-x$8VN$w)|(yXpl4tdZ</>]bjYTLD |n{W}:*0TT%t3Z7');

define('LOGGED_IN_SALT',   'kNSRAa`]aaRN^kF1}pEemyYFv1efT38^l|IhtG0<}R2DaYAOn13Pq8k=H<%Lg~Q4');

define('NONCE_SALT',       'hV-BSkoCO=pD@i$QU3UD,uW?>Cv:n).ngnK.s@uRIs*-t,$6bSWVquXE2}d]ln^#');


/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';


/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
