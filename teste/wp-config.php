<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'ingridix_bdteste');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'ingridix_testbd');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', 'teste123456');

/** Nome do host do MySQL */
define('DB_HOST', '108.167.132.64');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'H} (OjiLO4DFYB&%3E%dhQ;p.d|VHlND)c|(<k@GQr;L!kaErjAzp5z.{mC23+M3');
define('SECURE_AUTH_KEY',  'i`Jy?.Fr_A%(>yn7H61HT6)B^)v^or^Xm+2XfO<LTZP3ZEW+s)h71|D^}HJW`J:)');
define('LOGGED_IN_KEY',    '*#l11q+hBKfz<!)*e8&1VO-xwaW/f{Vn 8s3V!}J$p|!|a/V0f#-dTBXgioC|kfc');
define('NONCE_KEY',        'P.TQV2{jqv]Gmi9Ey>j<okztFoIPo}_2gJzlwQPpp9( 5 _kWX2@Z%SyVXFHHaUg');
define('AUTH_SALT',        ' MIBoQPa.%E18Y}e;xGo@t2P#|zn[Q`6Zx@P`h24`CAO>%k)Knq4Q`.RW`S`Hl^A');
define('SECURE_AUTH_SALT', 'FLWOPRRq<]#G`}Zllb3N.GZxE%>g~,TpR~>VQzTKe59ke*d5!pnnn+/h^#:xE=+i');
define('LOGGED_IN_SALT',   't#StdI}ET[3{[n3+asE]<[zg,AdyD^{DK-/p|^5=O#8~*rIr>Byu@#d7K++>]I= ');
define('NONCE_SALT',       'LJZLR=2P?iP5N7WWvRp.ivPLW)9MCR*/@QY[$*@O6B8{QQo2/4_D.wJhG>wV [BI');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
