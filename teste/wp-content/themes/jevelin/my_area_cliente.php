<?php
/*
Template Name: Área cliente
*/
if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}
get_header();
$current_user = wp_get_current_user();
$cpf_paciente = get_user_meta(  $current_user->id , 'billing_cpf', true );

?>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <style>
        .area-menu{
            background: #EAECED;
            text-align: left;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
            padding: 0;
            font-size: 14px;
            font-weight: bolder;
            color: #3e3e3e;
            padding-top: 13px;
            height: 350px;
        }

        .area-menu li{
            display: block;
            padding: 1px 15px 5px 17px;

        }

        .area-menu-footer{
            background: #B3956B;
            padding: 7px 0 0 0;
            position: absolute;
            bottom: 11px;
            width: 100%;

        }

        .area-menu-footer li, .area-menu-footer li a{
            color: #FFFFFF;
            font-size: 12px;
            text-decoration: underline;
        }

        .col-md-3{
            padding: 0 !important;
        }

        .area-content-list{
            margin-top:20px;
        }

        .area-content-all{
            border: 1px solid #E6BA7C;
            background: #FFFFFF;
            -webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            -moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
            height: 350px;
            padding-top: 15px
        }

        .area-content-alert{
            background: #A5C48E;

        }
        .area-content-alert h2{
            color: #FFFFFF;
            font-size: 25px;
            padding: 10px;
        }
        .div-list{
            background: #F5F5F5;

            font-size: 13px;
            font-weight: bolder;
        }
        .centered {
            margin: 0 auto !important;
            float: none !important;
        }
        .no-padding{
            padding: 0;
        }
        .btn-ingridix-cli{
            width: 27.4%;
            height: 50px;
            padding: 12px 10px 8px 10px;
            border-radius: 40px;
            background: #ECBB71;
            color: #FFFFFF;
            font-weight: bolder;
            font-size: 17px;
            margin-bottom: 15px;
            cursor: pointer;
        }
        .nome-logado{
            text-align: left;
            color: #555555;
            margin-bottom: 15px;
        }
        #wrapper > .sh-page-layout-default{
            padding: 30px 0 ;
        }
    </style>
    <center>
        <div class="container">

            <div class="row">

                <div class="col-md-11 centered">
                    <div class="col-md-11 no-padding">
                        <div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área cliente" onclick='location.href="<?= get_site_url().'/area-cliente'?>"'>ÁREA DO CLIENTE</div>
                    </div>
                    <div class="col-md-11 nome-logado">
                        Olá <b><?= $current_user->user_login; ?></b>
                    </div>
                    <div class="col-md-3">
                        <ul class="area-menu list-unstyled">
                            <li>RECEITAS ANTERIORES</li>
                            <li><a href="http://ingridix.com.br/duvidas-sobre-o-uso" target="_blank">DÚVIDAS SOBRE O USO</a></li>
                            <li><br></li>


                            <div class="area-menu-footer">
                                <!--<li>MEUS DADOS DE CADASTRO</li>-->
                                <li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
                            </div>
                        </ul>
                    </div>
					
					<?php
					$meus_pacientes = $rows = $wpdb->get_results( "select DISTINCT mr.id_receita_med, mr.id_ref, cp.nome_paciente, wu.user_login from cadastros_pacientes cp join wp_usermeta wum join minhas_receitas mr join wp_users wu
where cp.cpf_paciente = wum.meta_value and wum.meta_value = '".$cpf_paciente."' and mr.id_pac = cp.id_paciente and mr.id_med = wu.ID and mr.status = 0" );
					$qtd = count($meus_pacientes);
					
					?>
                    <div class="col-md-9 area-content">
                        <div class="col-md-12 area-content-all pull-right">
							<?php
							if($qtd > 0):
								?>
                                <div class="col-md-12 area-content-alert">
                                    <h2>VOCÊ TEM NOVA RECEITA!</h2>
                                </div>
								<?php
							else:
								?>
                                <div class="col-md-12 area-content-alert">
                                    <h2>VOCÊ NÂO POSSUI RECEITAS!</h2>
                                </div>
								<?php
							endif;
							?>
                            <div class="col-md-12 area-content-list">
								<?php
								foreach ($meus_pacientes as $row):
									?>

                                    <div class="col-md-6 div-list" style="color: #3e3e3e; text-transform: uppercase;">DR. <?= $row->user_login; ?></div>
                                    <div class="col-md-6 div-list" ><a href="<?= get_site_url().'/produto/?idr='.$row->id_receita_med.'&idrr='.$row->id_ref; ?>" style="color: #249B60">EFETUAR COMPRA</a></div>
									
									<?php
								endforeach;
								?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </center>

<?php
get_footer();
?>