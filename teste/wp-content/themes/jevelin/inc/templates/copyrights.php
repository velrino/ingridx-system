
<style>
    .contato-ingridix{
        width: 100%;
        height: 260px;
        background: #898989;
        text-align: center;
    }

    .contato-ingridix ul{
        display: inline-block;
        text-align: initial;
    }

    .contato-ingridix ul{
     list-style: none;
    }
    
    .social-ingridix{
        width: 400px;
        margin-top: 50px;
        text-align: center;
        margin-left: auto;
        margin-right: auto;
    }

    .social-ingridix a{
        margin: 0 14px;
    }
    
    .cadastresse-ingridix{
        text-align: center;
        margin: 10px;
        font-size: 18px;
        color: #FFFFFF;
    }
    
    .emailpara-ingridix{
        margin: 10px;
        margin-left: auto;
        margin-right: auto;

    }
    
    .formconteudo{
        display: inline-flex;
        align-content: center;
        text-align: center;
    }
    
    .inputconteudo{
        width: 300px;
        height: 50px;
        background: #FFFFFF;
        border-radius: 50px;
        margin-left: 60px;
        align-content: center;
        display: flex;
        padding: 10px 0 0 20px;

    }

    .inputconteudo input, .inputconteudo input:focus{
        outline: none;
        border: none;
        font-size: 16px;
    }
    
    .enviarconteudo{
        width: 100px;
        height: 50px;
        background: #6B369E;
        border-radius: 50px;
        z-index: 999;
        position: relative;
        left: -60px;
        color: #ffffff;
        font-weight: bolder;
        padding-top: 13px;
        cursor: pointer;
    }
    
    .menufooter-ingridix{
        margin: 10px;
        text-align: center;
    }
    
    .menufooter-ingridix a{
        color: #FFFFFF;
        border-right: 1px solid #FFFFFF;
        padding: 0 10px;
        font-size: 14px;
    }

    .menufooter-ingridix a:last-child{
        border-right: none;
    }

    .menufooter-ingridix a:hover{
        color: #FFFFFF;
    }
</style>

<div class="contato-ingridix">
    
    <ul>
        
        <li class="social-ingridix">
            <a title="Facebook" href=""><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/07/iconfb.png"></a>
            <a title="Youtube" href=""><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/07/iconyt.png"></a>
            <a title="Instagran" href=""><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/07/iconinsta.png"></a>
            <a title="Whatsapp" href=""><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/07/iconwapp.png"></a>
        </li>
        
        <li class="cadastresse-ingridix"><b>Cadastre-se para receber nossas novidades</b></li>
        
        <li class="emailpara-ingridix">
            <form method="post">
                
                <div class="formconteudo">
                    <div class="inputconteudo"><input type="text" name="emailingridix" id="emailingridix" value=""></div>
                    <div class="enviarconteudo">ENVIAR</div>
                </div>
                
            </form>
        </li>
        
        <li class="menufooter-ingridix">
            <a title="CONTATO" href="">CONTATO</a>
            <a title="BLOG" href="">BLOG</a>
            <a title="A INGRIDIX" href="">A INGRIDIX</a>
            <a title="ENTREGA" href="">ENTREGA</a>
        </li>
    </ul>
    
</div>

<div class="sh-copyrights-text" style="color: #FFFFFF;text-align: center">
    <p style="padding: 20px 0 0 0 !important;margin: 0; font-size: 15px"><b>INGRIDIX - SAÚDE E BEM ESTAR DE DENTRO PARA FORA</b></p>
    <p style="padding: 0 0 15px 0; font-size: 13px">DESENVOLVIDO POR AGENCIA BOWIE</p>
</div>

