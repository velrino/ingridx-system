<?php
/*
Template Name: Receita salva
*/

if(!is_user_logged_in()){
	wp_redirect( get_site_url()."/acesso-cliente/" );
}

global $wpdb;
get_header();
$current_user = wp_get_current_user();

//Dados do paciente
$id_receita    = $_GET['idr'];
$id_referencia = $_GET['idrr'];
$id_pac        = isset($_GET['idp']) ? $_GET['idp'] : "";

$user_id    = $current_user->ID;

if(empty($id_pac)){
	
	$rows = $wpdb->get_results( "select DISTINCT mr.id_produto, wp.post_title, mr.qtd_produto from receitas_medicos rm join minhas_receitas mr join referencia_receitas rr join wp_posts wp
where rm.id_receita = mr.id_receita_med and mr.id_ref = rr.id_ref
      and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_receita" );
	
}else{
	
	$user_id    = $current_user->ID;
	$rows = $wpdb->get_results( "select DISTINCT mr.id_produto, wp.post_title, mr.qtd_produto, cp.nome_paciente, cp.cpf_paciente from receitas_medicos rm join minhas_receitas mr join referencia_receitas rr join wp_posts wp join cadastros_pacientes cp
where rm.id_receita = mr.id_receita_med and mr.id_ref = rr.id_ref and cp.id_paciente = rr.id_pac
      and mr.id_produto = wp.ID and wp.post_type = 'product' and rm.id_receita = $id_receita" );
	
	
}

?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
    .area-menu{
        background: #EAECED;
        text-align: left;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        padding: 0;
        font-size: 14px;
        font-weight: bolder;
        color: #3e3e3e;
        padding-top: 13px;
        height: 410px;
    }

    .area-menu li{
        display: block;
        padding: 1px 15px 5px 17px;

    }

    .area-menu-footer{
        background: #334C5A;
        padding: 7px 0 0 0;
        position: absolute;
        bottom: 11px;
        width: 100%;

    }

    .area-menu-footer li{
        color: #FFFFFF;
        font-size: 12px;
        padding: 1px 15px 5px 17px;
        text-decoration: underline;
    }

    .area-menu-footer li a, .area-menu-footer li a:hover{
        color: #FFFFFF !important;
    }

    .col-md-3{
        padding: 0 !important;
    }

    .area-content-list{
        margin-top:20px;
    }

    .area-content-all{
        border: 1px solid #8BB5CF;
        background: #FFFFFF;
        -webkit-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        -moz-box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        box-shadow: 7px 11px 13px -6px rgba(0,0,0,0.62);
        height: 410px;
        padding: 0;
    }

    .area-content-alert{
        background: #8BB4D0;
        color: #FFFFFF;
        font-weight: bolder;
        text-align: left;
        padding-top: 16px;
        font-size: 13px;
    }
    .area-content-alert span{
        width: 100%;
        height: auto;
        padding: 4px;
        background: #BCD1DE;
        color: #555555;
        margin-left: 10px;
    }
    .area-content-alert h2{
        color: #FFFFFF;
        font-size: 25px;
        padding: 5px;
    }
    .div-list{
        padding: 5px;
        font-size: 13px;

    }
    .div-list p{
        margin: 0;
        text-align: left;
        color: #555555 ;
    }

    .pagamento{
        height: 120px;
        border: 1px solid #82AC63;
        margin-bottom: 25px;
        padding: 30px 6px 0 6px;
    }
    .pagamento h3{
        font-size: 22px;
        color: #2A654F;
    }

    .centered {
        margin: 0 auto !important;
        float: none !important;
    }
    .no-padding{
        padding: 0;
    }
    .btn-ingridix-cli{
        width: 27.4%;
        height: 50px;
        padding: 12px 10px 8px 10px;
        border-radius: 40px;
        background: #79B8DB;
        color: #FFFFFF;
        font-weight: bolder;
        font-size: 17px;
        margin-bottom: 15px;
        cursor: poi;
    }
    .btn-ingridix{
        height: 35px;
        padding: 9px 10px 8px 10px;
        border-radius: 40px;
        background: #ECBB71;
        color: #FFFFFF;
        font-weight: bolder;
        font-size: 13px;
        margin-bottom: 5px;
    }

    .btn-ingridix a, .btn-ingridix a:hover{
        color: #FFFFFF;
    }
    .nome-logado{
        text-align: left;
        color: #3C505D;
        margin-bottom: 15px;
    }
    .nome-logado a, .nome-logado a:hover{
        color: #3C505D;
    }
    #wrapper > .sh-page-layout-default{
        padding: 30px 0 ;
    }

    label{
        color: #FFFFFF;
        margin-right: 15px;
    }
    .imprimir-color{
        background: #384C59;
    }
    .enviar-email-color{
        background: #235997;
    }
    .efetuar-color{
        background: #84B8D7;
    }
    .list-receita li{
        color: #555555;
        font-size: 12px;
        margin-bottom: 5px;
        text-align: left;
    }
    .list-receita li span{
        float: right;
    }
    .block-cepa-content{
        background: #73818A;
        margin: 0 0 5px 0;
        padding: 4px;
    }
    .content-count{
        font-weight: bolder;
        line-height: 16px;
        margin-top: 6px;
        font-size: 12px;
        color: #FFFFFF;
    }
    .square-count{
        background: #FFFFFF;
        font-size: 32px;
        margin: 0;
        padding: 0;
        color: #555555;
    }
    .receitas{
        padding-left: 0;
        margin-top: 10px;
        text-align: left;

    }
    .receitas a, .receitas a:hover{
        color: #555555;
        text-decoration: none;
        font-size: 13px;
        font-weight: bolder;
    }
    .ingrdientes-list{

        color: #292929;
    }
    .salvar-receita{
        cursor: pointer;
    }
    /**************************/
    input[type="text"]{
        padding: 0 !important;
    }
    .count-input {
        position: relative;
        width: 60px;
    }
    .count-input span{
        position: relative;
        top: -6px;
    }
    .count-input input {
        width: 20px;
        height: 20px;
        border-radius: 2px;
        text-align: center;
        color: #000000;
        background: #DBE7EE;

    }
    .count-input input:focus {
        outline: none;
    }
    .count-input .incr-btn {
        display: block;
        position: absolute;
        width: 20px;
        height: 20px;
        font-size: 20px;
        font-weight: 300;
        text-align: center;
        line-height: 30px;
        top: 15px;
        right: 0;
        margin-top: -15px;
        text-decoration:none;
        background: #8BB5D0;
    }
    .count-input .incr-btn:first-child {
        right: auto;
        left: 0;
    }
    .count-input.count-input-sm {
        max-width: 125px;
    }
    .count-input.count-input-sm input {
        height: 36px;
    }
    .count-input.count-input-lg {
        max-width: 200px;
    }
    .count-input.count-input-lg input {
        height: 70px;
        border-radius: 3px;
    }

    .ingrdientes-list div:nth-child(2){
        text-align: left;
        padding-left: 10px;
    }

    .area-menu a, .area-menu a:hover{
        color: #3e3e3e;
        text-align: none;
    }

    .active{
        color: #337ab7 !important;
    }
</style>
<center>
    <div class="container">

        <div class="row">

            <div class="col-md-11 centered">

                <div class="col-md-11 no-padding">
                    <div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área médico" onclick='location.href="<?= get_site_url().'/area-medico'?>"'>INGRIDIX LAB</div>
                </div>
                <div class="col-md-3 nome-logado">
                    Olá <b>Dr. <?= $current_user->user_login; ?></b>
                </div>
                <div class="col-md-8 nome-logado">
                    <b><a >&nbsp;&nbsp;&nbsp;</a></b>
                </div>
                <div class="col-md-3">
                    <ul class="area-menu list-unstyled">
                        <li><a href="<?= get_site_url().'/cadastrar-novo-paciente'?>">CADASTAR NOVO PACIENTE</a></li>
                        <li><a data-toggle="modal" data-target="#novareceitaModal" data-whatever="@mdo">CRIAR NOVA FÓRMULA</a></li>
                        <li><a href="<?= get_site_url().'/area-medico'?>">RECEITAS ANTERIORES</a></li>
                        <li><a href="http://ingridix.com.br/duvidas-sobre-o-uso" target="_blank">DÚVIDAS SOBRE O USO</a></li>
                        <li><a href="<?= get_site_url().'/minhas-receitas'?>">MINHAS RECEITAS</a></li>
                        <li><br></li>


                        <div class="area-menu-footer">
                            <!--<li>MEUS DADOS DE CADASTRO</li>-->
                            <li><a href="<?php echo wp_logout_url(get_site_url()."/acesso-cliente"); ?>">DESCONECTAR</a></li>
                        </div>
                    </ul>
                </div>

                <div class="col-md-9 area-content">
                    <div class="col-md-12 area-content-all pull-right">
                        <div class="col-md-12 area-content-alert">
                            <div class="form-group col-md-7">
                                PACIENTE <span style="padding-right: 200px;"><?= $rows[0]->nome_paciente?></span>
                            </div>
                            <div class="form-group col-md-4">
                                CPF <span style="padding-right: 40px;"><?= $rows[0]->cpf_paciente?></span>
                            </div>
                        </div>

                        <div class="col-md-12 area-content-list">
                            <div class="col-md-4 ">
                                <div class="pagamento ">
                                    <h3>RECEITA SALVA COM SUCESSO!</h3>
                                </div>
                                <div class="button button-primary button-large btn-ingridix imprimir-color">IMPRIMIR RECEITA</div>
                                <div class="button button-primary button-large btn-ingridix imprimir-color">SALVAR COMO PDF</div>
                                <div class="button button-primary button-large btn-ingridix efetuar-color">ENVIAR PARA PACIENTE</div>
                                <div class="button button-primary button-large btn-ingridix enviar-email-color"><a href="<?= get_site_url().'/nova-formula/';?>">FAZER NOVA RECEITA</a></div>
                            </div>
                            <div class="col-md-8 div-list">
                                <div class="col-md-12" style="margin-bottom: 10px">
                                    <p><b>Composto probiótico:</b> 30 cápsulas contendo:</p>
                                    <p>Cada cápsula contém:</p>
                                </div>
                                <div class="col-md-12 ">
                                    <ul class="list-unstyled list-receita">
										
										<?php
										
										foreach($rows as $row):
											
											$cepa = $row->post_title;
											$lenghtCepa = strlen($cepa);
											$qtdDot = 120 - $lenghtCepa;
											
											?>

                                            <li><?php echo $row->post_title;
												echo "<span>";
												for($i=0;$i < $qtdDot; $i++){
													echo ".";
												}
												
												?> <?= "  ". $row->qtd_produto; ?> BHL/UFC</span>

                                            </li>
											
											<?php
										endforeach;
										?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</center>

<?php
get_footer();
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(function(){

        $(".salvar-receita").click(function(){

            location.href="<?= get_site_url().'/receita-salva'?>";

        });

        $(".incr-btn").on("click", function (e) {
            var $button = $(this);
            var oldValue = $button.parent().find('.quantity').val();
            $button.parent().find('.incr-btn[data-action="decrease"]').removeClass('inactive');
            if ($button.data('action') == "increase") {
                var newVal = parseFloat(oldValue) + 1;
            } else {
                // Don't allow decrementing below 1
                if (oldValue > 1) {
                    var newVal = parseFloat(oldValue) - 1;
                } else {
                    newVal = 0;
                    $button.addClass('inactive');
                }
            }
            $button.parent().find('.quantity').val(newVal);
            e.preventDefault();
        });

    })
</script>
