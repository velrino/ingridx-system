<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

wc_print_notices();

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
	echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) );
	return;
}

?>

<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
	
	<?php if ( $checkout->get_checkout_fields() ) : ?>
		
		<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

        <div class="col2-set" id="customer_details">
            <div class="col-1">
				<?php do_action( 'woocommerce_checkout_billing' ); ?>
            </div>

            <div class="col-2">
				<?php do_action( 'woocommerce_checkout_shipping' ); ?>
            </div>

            <div id="produtos_hidden">
				<?php
				global $woocommerce;
				$items = $woocommerce->cart->get_cart();
				
				$total = 0;
				$_product =  wc_get_product(941);
				$itemNumero = 0;
		  
				foreach($items as $item => $values) {
					$itemNumer++;
					$_product =  wc_get_product( $values['data']->get_id());
					$produtoId = $values['data']->get_id();
					
					$qty = $values['quantity'];
					$price = get_post_meta($values['product_id'] , '_price', true);
					
					?>
					<input type="hidden" id="produto_<?= $itemNumer; ?>" name="produto_<?= $itemNumer; ?>" data-prod-id="<?= $produtoId; ?>" data-prod-qtd="<?= $qty?>" >
					<?php
					
					$total += $qty * $price;
			
				}
				
				?>
            </div>
            <input type="hidden" id="referenciaReceita" name="referenciaReceita" value="<?= $_GET['idrr']; ?>" >
            <input type="hidden" id="itemNumero" name="itemNumero" value="<?= $itemNumer; ?>" >
            <input type="hidden" id="total" name="total" value="<?= $total; ?>" >
        </div>
		
		<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
	
	<?php endif; ?>

    <h3 id="order_review_heading"><?php _e( 'Your order', 'woocommerce' ); ?></h3>
	
	<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

    <div id="order_review" class="woocommerce-checkout-review-order">
		<?php do_action( 'woocommerce_checkout_order_review' ); ?>
    </div>
	
	<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

</form>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
