<?php
/**
 * Checkout Payment Section
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.5.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_ajax() ) {
	do_action( 'woocommerce_review_order_before_payment' );
}
?>

    <div id="payment" class="woocommerce-checkout-payment">
        <ul class="wc_payment_methods payment_methods methods">
         
            <li class="wc_payment_method payment_method_paypal">
                <input  type="radio" checked  value="paypal" style="display: inline !important;margin: 0 1em 0 0 !important;">

                <label for="payment_method_paypal">
                    Ultrapag </label>
                <div class="payment_box payment_method_paypal" >
                    <p>Pague com Ultrapag; você pode pagar com o seu cartão de crédito.</p>
                </div>
            </li>
        </ul>
        <div class="form-row place-order">
            <noscript>
                Seu navegador não suporta JavaScript ou ele está desativado, por favor clique no botão &lt;em&gt;Atualizar&lt;/em&gt; antes de finalizar seu pedido. Você pode ser cobrado mais do que a quantidade indicada acima, se você deixar de clicar.			&lt;br/&gt;&lt;input type="submit" class="button alt" name="woocommerce_checkout_update_totals" value="Atualizar" /&gt;
            </noscript>
            
            <input class="button alt" id="place_order" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" value="Finalizar compra" data-value="Finalizar compra" style="text-align: center">
            
    	</div>
   
    </div>

    <style>
        .modal-dialog input{
            padding: 0px 5px 0px 5px;
            font-size: 16px;
            height: 45px;

        }

        .modal-dialog .btn.cancelar{
            background-color: #F0BE60 !important;
            color: #FFFFFF;
        }
        .modal-dialog .btn.comprar{
            background-color: #15bee4 !important;
            color: #FFFFFF;
        }

        select {
            line-height: 40px !important;
            height: 45px !important;
            padding: 0 5px 10px 7px !important;
        }

        hr{
            margin-top: 0 !important;
        }

        .inline-flex{
            display: inline-flex;
        }

        .inline-flex input{
            width: 40px;
            margin-right: 10px;
            text-align: center;
        }

        #cartao_codigo{
            width: 100px;
        }
    </style>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="border: 4px solid #ccc;">
                <div class="modal-header" style="background: #f1f1f1; ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h2 class="modal-title" id="exampleModalLabel" style="color: #6C3B97">Pagamento</h2>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="cartao_credito" class="control-label" >Número do cartão*</label>
                                    <input type="text" class="form-control" name="cartao_credito" id="cartao_credito" maxLength="19" minlength="13" >
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="recipient-name" class="control-label" >Data de validade*</label>
                                <div class="col-md-5">
                                    <div class="form-group inline-flex" >
                                        <input type="text" class="form-control" name="cartao_mes" id="cartao_mes" maxlength="2" placeholder="MM" >
                                        <input type="text" class="form-control" name="cartao_ano" id="cartao_ano" maxlength="2" placeholder="AA">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-7">
                                <div class="form-group">
                                    <label for="cartao_nome" class="control-label" >Nome do dono do cartão</label>
                                    <input type="text" class="form-control" name="catao_nome" id="catao_nome" >
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="cartao_codigo" class="control-label">Código de segurança</label>

                                    <div class="inline-flex">
                                        <input type="text" class="form-control" name="cartao_codigo" id="cartao_codigo" maxlength="3">
                                        <i class="icon-info sh-alert-icon" style="margin-top: 5px;"></i>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">

                            <div class="col-md-12">
                                <h4 style="color: #6C3B97">Dados do dono do cartão</h4>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cartao_cpf" class="control-label" >CPF do dono do cartão</label>
                                    <input type="text" class="form-control" name="cartao_cpf" id="cartao_cpf" >
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="cartao_celular" class="control-label">Celular do dono do cartão</label>
                                    <input type="text" class="form-control" name="cartao_celular" id="cartao_celular">
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="cartao_nascimento" class="control-label">Data de nascimento</label>
                                    <input type="text" class="form-control" name="cartao_nascimento" id="cartao_nascimento" style="margin-bottom: 0">
                                    <small>Ex.: 20/05/1980</small>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">

                            <div class="col-md-12">
                                <h4 style="color: #6C3B97">Parcelamento</h4>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label" >Pague em até 1x sem juros</label>
                                    <select class="form-control">
                                        <option selected >1x sem juros</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn cancelar" data-dismiss="modal">Cancelar compra</button>
                    <button type="button" class="btn comprar">Confirmar pagamento</button>
                </div>
            </div>
        </div>
    </div>
    <script src='<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.min.js'></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.validate.min.js"></script>
    <script src="<?php echo get_site_url()?>/wp-content/themes/jevelin/js/jquery.maskedinput.min.js"></script>


    <script>

        $(function(){

            $("#cartao_nascimento").mask( "99/99/9999" );
            $("#cartao_cpf").mask( "999.999.999-99" );
            $("#cartao_celular").mask( "(99) 99999-9999" );

            $('#exampleModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var recipient = button.data('whatever') // Extract info from data-* attributes
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('.modal-title').text('New message to ' + recipient)
                modal.find('.modal-body input').val(recipient)
            })

            $(".comprar").click(function(){

                var formCheckout      = $('form[name="checkout"]').serialize();
                var cartao_credito    = $("#cartao_credito").val();
                var cartao_mes        = $("#cartao_mes").val();
                var cartao_ano        = $("#cartao_ano").val();
                var cartao_codigo     = $("#cartao_codigo").val();
                var cartao_nome       = $("#catao_nome").val();

                var referenciaReceita = $("#referenciaReceita").val();
                var itemNumero        = $("#itemNumero").val();
                var total             = $("#total").val();

                var urlProd           = "";
                var numerodeid        = 0;

                $("#produtos_hidden input").each(function(){
                    numerodeid++;
                    var prodId  = $(this).attr("data-prod-id");
                    var prodQtd = $(this).attr("data-prod-qtd");

                    urlProd += "&prodId_"+ numerodeid +"=" + prodId + "&prodQtd_"+ numerodeid +"=" + prodQtd ;
                });

                //Envia para função ajax
                $.ajax({
                    url: '<?= get_site_url()."/functions-ajax";?>',
                    type: "post",
                    data: formCheckout + "&woocommerce_confirmacao=ok&cartao_credito=" +
                     cartao_credito + "&cartao_mes=" + cartao_mes + "&cartao_ano=" + cartao_ano + "&cartao_codigo=" + cartao_codigo +
                     "&total=" + total + "&cartao_nome=" + cartao_nome + urlProd + "&itemNumero=" + itemNumero + "&referenciaReceita=" + referenciaReceita ,
                     dataType: "json",
                    success: function(data){
                        // alert(data.status);
                       // alert(data)
                        location.href="" + data.url + "";
                    
                    }
                })
            });
        })

    </script>

<?php

