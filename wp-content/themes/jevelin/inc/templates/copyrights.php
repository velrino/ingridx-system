
<style>
    .contato-ingridix{
        width: 100%;
        height: 260px;
        background: #898989;
        text-align: center;
    }

    .contato-ingridix ul{
        display: inline-block;
        text-align: initial;
    }

    .contato-ingridix ul{
     list-style: none;
    }
    
    .social-ingridix{
        width: 400px;
        margin-top: 50px;
        text-align: center;
        margin-left: auto;
        margin-right: auto;
    }

    .social-ingridix a{
        margin: 0 10px;
    }
    
    .cadastresse-ingridix{
        text-align: center;
        margin: 10px;
        font-size: 18px;
        color: #FFFFFF;
    }
    
    .emailpara-ingridix{
        text-align: center !important;
        margin: 15px;
        margin-left: auto;
        margin-right: auto;

    }
    
    .formconteudo{
        display: inline-flex;
        align-content: center;
        text-align: center;
    }
    
    .inputconteudo{
        width: 300px;
        height: 50px;
        background: #FFFFFF;
        border-radius: 50px;
        margin-left: 60px;
        align-content: center;
        display: flex;
        padding: 10px 0 0 20px;

    }

    .inputconteudo input, .inputconteudo input:focus{
        outline: none;
        border: none;
        font-size: 16px;
    }
    
    .enviarconteudo{
        width: 100px;
        height: 50px;
        background: #6B369E;
        border-radius: 50px;
        z-index: 999;
        position: relative;
        left: -60px;
        color: #ffffff;
        font-weight: bolder;
        padding-top: 13px;
        cursor: pointer;
    }
    
    .menufooter-ingridix{
        margin: 10px;
        text-align: center;
    }
    
    .menufooter-ingridix a{
        color: #FFFFFF;
        border-right: 1px solid #FFFFFF;
        padding: 0 10px;
        font-size: 14px;
    }

    .menufooter-ingridix a:last-child{
        border-right: none;
    }

    .menufooter-ingridix a:hover{
        color: #FFFFFF;
    }

    
    /* Breakpoints */
    @media only screen and (min-width: 320px) and (max-width: 620px) {

        .social-ingridix{
            width: 100%;
        }
      
        .contato-ingridix ul {
            width: 100% !important;
            list-style: none;
            margin: 0;
            padding: 0;
        }
        .cadastresse-ingridix{
            font-size: 15px !important;
            line-height: 18px !important;
        }

        .formconteudo{
            width: 100% !important;
        }
        .inputconteudo{
            width: 100% !important;
        }

        .inputconteudo input, .inputconteudo input:focus {
            font-size: 14px !important;
            padding-left: 0 !important;
            margin-left: -5px !important;
        }
        .enviarconteudo{
            font-size: 13px !important;
        }

        .sh-copyrights-text p{
            font-size: 14px !important;
        }
    }

    @media only screen and (min-width: 621px) and (max-width: 940px) {

    }

    @media only screen and (min-width: 941px) and (max-width: 1260px) {
    
    }

    @media only screen and (min-width: 1261px) {
    
    }
    
</style>

<div class="contato-ingridix">
    
    <ul>
        <li class="social-ingridix">
            <a title="Facebook"  target="_blank" href="https://www.facebook.com/Ingridix-128006271187921/"><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/07/iconfb.png"></a>
            <a title="Linkedin"  target="_blank" href="https://www.linkedin.com/company-beta/25073661"><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/09/linkedinIngridix.png"></a>
            <a title="Instagran" target="_blank" href="https://www.instagram.com/ingridixlab/"><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/07/iconinsta.png"></a>
            <a title="Whatsapp"  target="_blank" href="https://api.whatsapp.com/send?phone=5511996990394&text=Ol%C3%A1,%20gostaria%20de%20fazer%20um%20pedido%20de%20um%20composto%20Ingridix"><img src="<?= get_site_url(); ?>/wp-content/uploads/2017/07/iconwapp.png"></a>
        </li>
        
        <li class="cadastresse-ingridix"><b>Cadastre abaixo seu e-mail para receber nossas novidades</b></li>
        
        <li class="emailpara-ingridix">
            <form method="post">
                
                <div class="formconteudo">
                    <div class="inputconteudo"><input type="text" name="emailingridix" id="emailingridix" value=""></div>
                    <div class="enviarconteudo">ENVIAR</div>
                </div>
                
            </form>
        </li>
        
        
    </ul>
    
</div>

<div class="sh-copyrights-text" style="color: #FFFFFF;text-align: center">
    <p style="padding: 20px 0 0 0 !important;margin: 0; font-size: 15px"><b>INGRIDIX - SAÚDE E BEM ESTAR DE DENTRO PARA FORA</b></p>
    <p style="padding: 0 0 15px 0; font-size: 13px">DESENVOLVIDO POR AGENCIA BOWIE</p>
</div>

