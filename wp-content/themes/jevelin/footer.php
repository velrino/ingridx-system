<?php
/**
 * Footer
 */

?>

<?php if( jevelin_post_option( get_the_ID(), 'page_layout' ) != 'full' ) : ?>
    </div>
<?php endif; ?>
</div>

<footer class="sh-footer" style="background: #21AA04; ">
	<?php if( jevelin_footer_enabled() == 'on' ) : ?>

        <div class="sh-footer-widgets">
            <div class="container">
                <div class="sh-footer-columns">
					<?php
					/* Show theme footer widgets */
					dynamic_sidebar( 'footer-widgets' );
					?>
                </div>
            </div>
        </div>
	
	<?php endif; ?>
	<?php
	if( jevelin_copyrights_enabled() == 'on' ) :
		/* Inlcude theme copyrights bar */
		get_template_part('inc/templates/copyrights' );
	endif;
	?>
</footer>
</div>


<?php if( jevelin_post_option( get_the_ID(), 'back_to_top' ) != 'none' ) :
	
	/* Inlcude back to top button HTML */
	get_template_part('inc/templates/back_to_top' );

endif; ?>
</div>

<?php wp_footer(); ?>

<style>
    .btn-ingridix-cli {
        width: 270px;
        height: 50px;
        padding: 12px 10px 8px 10px;
        border-radius: 40px;
        background: #79B8DB;
        color: #FFFFFF;
        font-weight: bolder;
        font-size: 17px;
        margin-bottom: 15px;
        cursor: pointer;
        text-align: center;
    }
</style>
<div class="modal fade" id="novareceitaModal" tabindex="-1" role="dialog" aria-labelledby="novareceitaModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border: 4px solid #ccc;">

            <div class="modal-header" style="background: #f1f1f1; ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="exampleModalLabel" style="color: #6C3B97">Escolha uma fórmula:</h2>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="col-md-6">
                            <div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área médico" onclick="location.href='<?= get_site_url();?>/nova-formula'">MANUAL</div>
                        </div>
                        <div class="col-md-6">
                            <div class="button button-primary button-large btn-ingridix-cli pull-left" title="Área médico" onclick="location.href='<?= get_site_url();?>/nova-formula-dinamica'">DINÂMICA</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $(function(){

        $('#novareceitaModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            //  modal.find('.modal-title').text('New message to ' + recipient)
            //   modal.find('.modal-body input').val(recipient)
        })

    })

</script>

</body>
</html>
